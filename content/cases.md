+++
date = "2016-01-02T19:42:54-04:00"
draft = false
title = "Case studies"
slug = "cases"
description = "Examples of Mayan EDMS usage."
type = "cases"

[[packages.list]]
    name = "California Public Utilities Commission, Energy Division, Utility Advice Letters and Related Documents"
    description = "The CPUC publishes electronic copies of official CPUC-generated proceeding documents, Advice Letters, and documents that are electronically filed by third parties during a proceeding. Decisions, Resolutions, Rulings, and other documents issued after June 2000 are available online."
    url = "https://cpucadviceletters.org"
    image_url = "/content/cases/cpucadviceletters.png"
    documents = "1,414"
    documentsDate = "2016-02-01"

[[packages.list]]
    name = "OPENode"
    description = "OPENode is open source web application for communities seeking answers for diverse problems in commercial, public or voluntary sectors. Based on flexible communication in nodes it helps to find solutions effectively and build smarter knowledgebase."
    url = "http://openode.net/"
    image_url = "/content/cases/openode.png"

[[packages.list]]
    name = "OGPE"
    description = "The Permit Management Office (PMO) is responsible for issuing final determinations and permits, licenses, inspections, certifications and planing authorization. Additional information: [1](https://plus.google.com/u/0/b/108413286958999778262/+Mayan-edms/posts/eMUDUsNS3pj) [2](https://plus.google.com/u/0/b/108413286958999778262/+Mayan-edms/posts/gEhb4SjhFHc)"
    url = "https://www.sip.pr.gov"
    image_url = "/content/cases/ogpe.jpg"
    documents = "23,000"
    documentsDate = "2013-04-01"

[[packages.list]]
    name = "Université de Montréal"
    description = "The School of Library and Information Science from the University of Montreal use Mayan EDMS as a lab tool for their ARV3054 course - digital archive management. Additional information: [1](http://cours.ebsi.umontreal.ca/planscours/diffusion/index.php?cours=arv3054) [2](https://arv3054.github.io/)"
    url = "https://github.com/ARV3054/docker-mayan-edms"
    image_url = "/content/cases/universite-montreal.png"

[[packages.list]]
    name = "CommonS Project"
    description = "An international co-learning and e-mentoring Community of Practice (CoP) devoted to improving the employability skills and work opportunities of participants producing remixed and localised OERs, i.e. Open Educational Resources freely available to everyone. Additional information: [1](https://github.com/gtoffoli/commons)" 
    url = "http://www.commonspaces.eu/"
    image_url = "/content/cases/commonspaces.jpg"
        
+++
