+++
date = "2016-01-02T19:42:54-04:00"
draft = false
title = "Ecosystem"
slug = "ecosystem"
description = "List of projects related to Mayan EDMS"
type = "ecosystem"

[[packages.list]]
    name = "Mayan EXIF"
    description = "Mayan EDMS app that extracts image documents' EXIF data."
    url = "https://pypi.python.org/pypi/mayan-exif"
    image_url = "https://gitlab.com/mayan-edms/exif/raw/master/contrib/art/logo.png"

[[packages.list]]
    name = "Mayan Document Renaming"
    description = "Automatic document renaming app for Mayan EDMS."
    url = "https://pypi.python.org/pypi/mayan-document_renaming"
    image_url = "https://gitlab.com/mayan-edms/document_renaming/raw/master/contrib/art/logo.png"

[[packages.list]]
    name = "Mayan EDMS website"
    description = "Statically generated website for Mayan EDMS."
    url = "https://gitlab.com/mayan-edms/website"
    image_url = "https://gitlab.com/mayan-edms/mayan-edms/raw/master/docs/_static/mayan_logo.png"

[[packages.list]]
    name = "Mayan EDMS fabric"
    description = "Fabric project to deploy Mayan EDMS."
    url = "https://gitlab.com/mayan-edms/mayan-fabric"
    image_url = "http://www.fabfile.org/_static/logo.png"

[[packages.list]]
    name = "Official docker container"
    description = "Docker file image for Mayan EDMS."
    url = "https://gitlab.com/mayan-edms/mayan-edms-docker"
    image_url = "http://www.docker.com/sites/all/themes/docker/assets/images/logo.png"

[[packages.list]]
    name = "Cookiecutter Mayan"
    description = "A cookiecutter template for creating Mayan EDMS apps quickly."
    url = "https://gitlab.com/mayan-edms/cookiecutter-mayan"
    image_url = "/content/ecosystem/cookiecutter-logo-large.png"

[[packages.list]]
    name = "Python API client"
    description = "Python client for the Mayan EDMS REST API."
    url = "https://pypi.python.org/pypi/mayan-api_client"
    image_url = "/content/ecosystem/python.png"

+++
